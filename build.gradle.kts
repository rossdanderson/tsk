plugins {
    kotlin("multiplatform") version "1.9.20" apply false
    kotlin("plugin.serialization") version "1.9.20" apply false
}

allprojects {
    group = "uk.co.coroutines.tsk"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
    }
}