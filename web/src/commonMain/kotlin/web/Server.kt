package web

import db.ID
import db.ILocalQueries
import db.IRemoteQueries
import db.orThrow
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*

class Server(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
    private val port: Int = 7990,
) {
    fun start() {
        embeddedServer(CIO, port) {
            install(ContentNegotiation) {
                json()
            }

            routing {
                // Cacheable
                get("/context") {
                    call.respond(remoteQueries.getContextNames().orThrow())
                }
                get("/context/{contextId}") {
                    val contextId: Long by call.parameters
                    call.respond(remoteQueries.getContextName(ID(contextId.toUInt())).orThrow())
                }
                get("/task/{contextId}") {
                    val contextId: Long by call.parameters
                    call.respond(
                        remoteQueries.getTasks(ID(contextId.toUInt()))
                            .orThrow()
                    )
                }
                get("/task/{contextId}/{taskId}") {
                    val contextId: Long by call.parameters
                    val taskId: Long by call.parameters
                    call.respond(
                        remoteQueries.getTask(ID(contextId.toUInt()), ID(taskId.toUInt()))
                            .orThrow()
                    )
                }

                // Uncacheable
            }
        }.start()
    }
}