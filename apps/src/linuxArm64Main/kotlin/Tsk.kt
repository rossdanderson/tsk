import common.Context
import web.Server

// https://github.com/ajalt/clikt/issues/404
fun main(args: Array<String>) {
    Context.run {
        Server(localQueries, remoteQueries).start()
    }
}
