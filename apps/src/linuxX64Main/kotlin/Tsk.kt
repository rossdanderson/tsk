import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal
import command.TskCli
import common.Context
import web.Server

fun main(args: Array<String>) {
    Context.run {
        TskCli(
            localQueries,
            remoteQueries,
            remoteQueries,
            dueParser,
            recurParser,
            listOf(
                object : CliktCommand(name = "serve", help = "Start web server") {
                    override fun run() {
                        Server(localQueries, remoteQueries).start()

                        // Ctrl-C not being handled...?
                        terminal.println("Press enter to exit")
                        terminal.readLineOrNull(true)
                    }
                },
            ),
        ).main(args)
    }
}
