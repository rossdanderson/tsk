import command.TskCli
import common.Context

fun main(args: Array<String>) {
    Context.run {
        TskCli(localQueries, remoteQueries, remoteQueries, dueParser, recurParser).main(args)
    }
}
