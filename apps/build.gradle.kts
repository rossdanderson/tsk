plugins {
    kotlin("multiplatform") version "1.9.20"
}

kotlin {
    val targets = listOf(linuxArm64(), linuxX64(), mingwX64(), macosArm64(), macosX64())

    targets.forEach { target ->
        target.binaries {
            executable {
                entryPoint = "main"
                baseName = "tsk"
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":core"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val linuxArm64Main by getting {
        }
        val linuxX64Main by getting {
        }
        val macosArm64Main by getting {
        }
        val macosX64Main by getting {
        }
        val mingwX64Main by getting {
        }

        listOf(linuxX64Main, macosArm64Main, macosX64Main, mingwX64Main).forEach {
            it.dependencies {
                implementation(project(":cli"))
            }
        }

        listOf(linuxArm64Main, linuxX64Main, macosArm64Main, macosX64Main).forEach {
            it.dependencies {
                implementation(project(":web"))
            }
        }
    }
}
