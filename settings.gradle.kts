rootProject.name = "tsk"

include(":core")
include(":cli")
include(":apps")
include(":web")