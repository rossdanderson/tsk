package command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.mordant.rendering.TextColors.red
import com.github.ajalt.mordant.rendering.TextColors.yellow
import com.github.ajalt.mordant.table.Borders.NONE
import com.github.ajalt.mordant.table.table
import common.systemTimeZone
import common.toHumanString
import db.*
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock.System
import kotlinx.datetime.Instant
import kotlinx.datetime.periodUntil
import kotlin.time.Duration.Companion.days

class List(
    private val localQueries: ILocalQueries,
    private val cacheableQueries: ICacheableQueries,
) : CliktCommand(help = "Lists tasks and their details") {

    private val showCompleted: Boolean by option("--completed", "-c", help = "Include completed tasks").flag()

    override fun run(): Unit = runBlocking {
        val now = System.now()
        localQueries.getCurrentContext().flatMap { contextName -> cacheableQueries.getTasks(contextName) }
            .onSuccess { tasks ->
                val filteredTasks = if (showCompleted) tasks
                else tasks.filterValues { task -> task.completed == null }

                terminal.println(
                    table {
                        tableBorders = NONE
                        cellBorders = NONE
                        header {
                            style(bold = true)
                            row {
                                cell("ID")
                                cell("Description")
                                cell("Due")
                                cell("Remaining")
                                if (showCompleted) cell("Completed")
                                cell("#Notes")
                                cell("Age")
                                cell("Recur")
                            }
                        }

                        body {
                            filteredTasks.entries
                                .asSequence()
                                .sortedWith(
                                    compareBy<Map.Entry<ID, Task>, Instant?>(nullsLast()) { (_, task) ->
                                        task.due?.completeBy?.takeIf { task.completed == null }
                                    }.thenByDescending(nullsLast()) { (_, task) -> task.completed }
                                )
                                .forEach { (id, task) ->
                                    row {
                                        if (task.completed != null) style(dim = true)

                                        cell(id)
                                        cell(task.description)

                                        val completeBy = task.due?.completeBy
                                        cell(completeBy?.toHumanString().orEmpty())

                                        cell(
                                            completeBy
                                                ?.takeIf { task.completed == null }
                                                ?.let { now.periodUntil(it, systemTimeZone).toHumanString(2) }
                                                .orEmpty()
                                        ) {
                                            if (task.completed == null) {
                                                style(
                                                    color = when {
                                                        completeBy == null -> null
                                                        now >= completeBy -> red
                                                        (now + 1.days) >= completeBy -> yellow
                                                        else -> null
                                                    }
                                                )
                                            }
                                        }
                                        if (showCompleted) cell(task.completed?.toHumanString().orEmpty())
                                        cell(task.notes.size.takeIf { it != 0 }?.toString().orEmpty())
                                        cell(task.created.periodUntil(now, systemTimeZone).toHumanString(1))
                                        cell(task.recur?.toHumanString().orEmpty())
                                    }
                                }
                        }
                    }
                )
            }
            .onFailure { terminal.warning(it) }
    }
}
