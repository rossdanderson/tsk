package command

import com.github.ajalt.clikt.completion.CompletionCommand
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.NoOpCliktCommand
import com.github.ajalt.clikt.core.subcommands
import command.context.Context
import command.notes.Note
import db.ICacheableQueries
import db.ILocalQueries
import db.IRemoteQueries
import parse.DueParser
import parse.RecurParser

class TskCli(
    localQueries: ILocalQueries,
    cacheableQueries: ICacheableQueries,
    remoteQueries: IRemoteQueries,
    dueParser: DueParser,
    recurParser: RecurParser,
    extraCommands: Iterable<CliktCommand> = emptyList(),
) : NoOpCliktCommand(printHelpOnEmptyArgs = true) {

    init {
        subcommands(
            buildList {
                add(Add(localQueries, remoteQueries, dueParser, recurParser))
                add(List(localQueries, cacheableQueries))
                add(Details(localQueries, cacheableQueries))
                add(Done(localQueries, remoteQueries))
                add(Note(localQueries, remoteQueries))
                add(Delete(localQueries, remoteQueries))
                add(Context(localQueries, cacheableQueries, remoteQueries))
                addAll(extraCommands)
                if (isCompletionSupported) add(CompletionCommand())
            }
        )
    }

    override fun aliases() = mapOf(
        "mk" to listOf("add"),
        "ls" to listOf("list"),
        "rm" to listOf("delete")
    )
}