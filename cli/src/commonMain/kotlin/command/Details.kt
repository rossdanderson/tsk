package command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.mordant.rendering.BorderType.Companion.SQUARE_DOUBLE_SECTION_SEPARATOR
import com.github.ajalt.mordant.rendering.OverflowWrap
import com.github.ajalt.mordant.rendering.TextAlign.CENTER
import com.github.ajalt.mordant.rendering.TextAlign.LEFT
import com.github.ajalt.mordant.rendering.TextColors.red
import com.github.ajalt.mordant.rendering.TextColors.yellow
import com.github.ajalt.mordant.rendering.Whitespace
import com.github.ajalt.mordant.table.Borders.LEFT_RIGHT
import com.github.ajalt.mordant.table.table
import common.systemTimeZone
import common.toHumanString
import db.*
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock.System
import kotlinx.datetime.periodUntil
import kotlin.Int.Companion.MAX_VALUE
import kotlin.time.Duration.Companion.days

class Details(
    private val localQueries: ILocalQueries,
    private val cacheableQueries: ICacheableQueries,
) : CliktCommand(help = "Show all details for a specific task") {

    private val id by argument(help = "The ID of the task to add a note to").id()

    override fun run(): Unit = runBlocking {
        val now = System.now()
        localQueries.getCurrentContext()
            .flatMap { contextName -> cacheableQueries.getTask(contextName, id) }
            .onSuccess { task ->
                terminal.println(
                    table {
                        borderType = SQUARE_DOUBLE_SECTION_SEPARATOR
                        header {
                            row {
                                cell("Due") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                val completeBy = task.due?.completeBy
                                cell(completeBy?.toHumanString().orEmpty()) {
                                    align = CENTER
                                }

                                cell("Remaining") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                cell(
                                    completeBy
                                        ?.takeIf { task.completed == null }
                                        ?.let { now.periodUntil(it, systemTimeZone).toHumanString(2) }
                                        .orEmpty()
                                ) {
                                    align = CENTER
                                    if (task.completed != null) {
                                        style(
                                            color = when {
                                                completeBy == null -> null
                                                now >= completeBy -> red
                                                (now + 1.days) >= completeBy -> yellow
                                                else -> null
                                            }
                                        )
                                    }
                                }

                                cell("Completed") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                cell(task.completed?.toHumanString().orEmpty()) {
                                    align = CENTER
                                }
                            }
                            row {
                                cell("Created") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                cell(task.created.toHumanString()) {
                                    align = CENTER
                                }

                                cell("Age") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                cell(task.created.periodUntil(now, systemTimeZone).toHumanString(1)) {
                                    align = CENTER
                                }

                                cell("Recur") {
                                    align = LEFT
                                    style(bold = true)
                                }
                                cell(task.recur?.toHumanString().orEmpty()) {
                                    align = CENTER
                                }
                            }
                        }

                        body {
                            row {
                                cell(task.description) {
                                    align = CENTER
                                    columnSpan = MAX_VALUE
                                    whitespace = Whitespace.NORMAL
                                    overflowWrap = OverflowWrap.NORMAL
                                    padding {
                                        top = 1
                                        bottom = 1
                                    }
                                }
                            }
                        }

                        if (task.notes.isNotEmpty()) {
                            footer {
                                row {
                                    cellBorders = LEFT_RIGHT
                                    cell("Notes") {
                                        align = LEFT
                                        style(bold = true)
                                        columnSpan = MAX_VALUE
                                    }
                                }
                                task.notes.entries
                                    .asSequence()
                                    .sortedBy { it.value.created }
                                    .forEach {
                                        row {
                                            cell(it.key)
                                            cell(it.value.created.toHumanString())
                                            cell(it.value.note) {
                                                align = CENTER
                                                columnSpan = MAX_VALUE
                                                whitespace = Whitespace.NORMAL
                                                overflowWrap = OverflowWrap.NORMAL
                                            }
                                        }
                                    }
                            }
                        }
                    }
                )
            }
            .onFailure { terminal.warning(it) }
    }
}
