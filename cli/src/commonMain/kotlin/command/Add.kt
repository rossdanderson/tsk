package command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import com.github.ajalt.clikt.parameters.options.required
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import parse.DueParser
import parse.RecurParser

class DueOptions(dueParser: DueParser, recurParser: RecurParser) : OptionGroup() {

    val due by option(help = dueParser.descriptions.joinToString())
        .convert {
            dueParser.parse(it, Clock.System)
                ?: fail("Does not match any of the valid formats - ${dueParser.descriptions.joinToString()}")
        }
        .required()

    val recur by option(help = recurParser.descriptions.joinToString())
        .convert {
            recurParser.parse(it)
                ?: fail("Does not match any of the valid formats - ${recurParser.descriptions.joinToString()}")
        }
}

class Add(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
    dueParser: DueParser,
    recurParser: RecurParser,
) : CliktCommand(help = "Add a task") {

    private val dueOptions by DueOptions(dueParser, recurParser).cooccurring()

    private val description by option(help = "A description of the task").prompt()

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .flatMap { contextId ->
                remoteQueries.createTask(
                    contextId = contextId,
                    description = description,
                    dueBy = dueOptions?.let { it.due to it.recur },
                )
            }
            .onSuccess { (id, _) -> terminal.success("Task $id '$description' created.") }
            .onFailure { terminal.warning(it) }
    }
}
