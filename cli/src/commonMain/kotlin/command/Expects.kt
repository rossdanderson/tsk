package command

import kotlinx.io.files.Path

expect val isCompletionSupported: Boolean
