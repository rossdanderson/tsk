package command

import com.github.ajalt.clikt.parameters.arguments.RawArgument
import com.github.ajalt.clikt.parameters.arguments.convert
import com.github.ajalt.mordant.terminal.Terminal
import db.ID

val terminal = Terminal()

fun RawArgument.id() = convert { ID(it.toUInt()) }