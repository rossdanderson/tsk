package command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import db.ILocalQueries
import db.IRemoteQueries
import db.onFailure
import db.onSuccess
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock

class Done(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Mark one or more tasks as complete", printHelpOnEmptyArgs = true) {

    private val ids by argument(help = "The IDs of the tasks to mark as complete").id().multiple(required = true)

    override fun run(): Unit = runBlocking {
        val now = Clock.System.now()

        localQueries.getCurrentContext()
            .onSuccess { contextName ->
                ids.forEach { id ->
                    remoteQueries.completeTask(contextName, id, now)
                        .onSuccess { (_, recurringTask) ->
                            if (recurringTask != null) terminal.success("Task $id marked as completed, and recurring task ${recurringTask.first} created.")
                            else terminal.success("Task $id marked as completed.")
                        }
                        .onFailure { terminal.warning(it) }
                }
            }
            .onFailure { terminal.warning(it) }
    }
}
