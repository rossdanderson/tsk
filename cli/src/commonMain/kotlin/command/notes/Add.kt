package command.notes

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import command.id
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock

class Add(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Add a note to a task", printHelpOnEmptyArgs = true) {

    private val id by argument(help = "The ID of the task to add a note to").id()

    private val note by option(help = "The note to add to the task").prompt()

    override fun run(): Unit = runBlocking {
        val now = Clock.System.now()
        localQueries.getCurrentContext()
            .flatMap { contextName -> remoteQueries.createTaskNote(contextName, id, now, note) }
            .onSuccess { terminal.success("Note added to task $id.") }
            .onFailure { terminal.warning(it) }
    }
}
