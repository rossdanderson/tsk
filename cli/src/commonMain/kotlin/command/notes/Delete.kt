package command.notes

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.mordant.terminal.YesNoPrompt
import command.id
import db.*
import kotlinx.coroutines.runBlocking

class Delete(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Delete one or more notes", printHelpOnEmptyArgs = true) {

    private val taskId by argument(help = "The ID of the task to delete the note from").id()

    private val noteIds by argument(help = "The IDs of the notes to delete").id().multiple(required = true)

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .flatMap { contextId -> remoteQueries.getTask(contextId, taskId).map { contextId to it } }
            .onSuccess { (contextId, task) ->
                noteIds.forEach { noteId ->
                    val note = task.notes[noteId]?.note
                    if (note == null) terminal.warning("Note $noteId does not exist.")
                    else if (YesNoPrompt(
                            "Are you sure you wish to delete note $noteId '$note' from task $taskId '${task.description}?",
                            terminal
                        ).ask() == true)
                        remoteQueries.deleteTaskNote(contextId, taskId, noteId)
                            .onSuccess { terminal.success("Note $noteId deleted.") }
                            .onFailure { terminal.warning(it) }
                }
            }
            .onFailure { terminal.warning(it) }
    }
}
