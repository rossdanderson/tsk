package command.notes

import com.github.ajalt.clikt.core.NoOpCliktCommand
import com.github.ajalt.clikt.core.subcommands
import db.ILocalQueries
import db.IRemoteQueries

class Note(
    localQueries: ILocalQueries,
    remoteQueries: IRemoteQueries,
) : NoOpCliktCommand(help = "Manage notes on a task", printHelpOnEmptyArgs = true) {
    init {
        subcommands(
            listOf(
                Add(localQueries, remoteQueries),
                Delete(localQueries, remoteQueries)
            )
        )
    }
}
