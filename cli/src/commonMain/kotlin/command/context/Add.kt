package command.context

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.check
import command.terminal
import db.ContextName
import db.IRemoteQueries
import db.onFailure
import db.onSuccess
import kotlinx.coroutines.runBlocking

class Add(
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Add a new context", printHelpOnEmptyArgs = true) {

    private val name by argument(help = "A name for the context")
        .check("value can only contain letters, numbers and -") { Regex("^[a-zA-Z0-9\\-]+$").matches(it) }

    override fun run(): Unit = runBlocking {
        remoteQueries.createContext(ContextName(name))
            .onSuccess { id -> terminal.success("Context $name ($id) created.") }
            .onFailure { terminal.warning(it) }
    }
}
