package command.context

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.check
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking

class Switch(
    private val localQueries: ILocalQueries,
    private val cacheableQueries: ICacheableQueries,
) : CliktCommand(help = "Switch the active context", printHelpOnEmptyArgs = true) {

    private val name by argument(help = "The context to select")
        .check("value can only contain letters, numbers and -") { Regex("^[a-zA-Z0-9\\-]+$").matches(it) }

    override fun run(): Unit = runBlocking {
        cacheableQueries.getContextNames()
            .flatMap { contexts ->
                contexts.asSequence().firstOrNull { context -> context.value.value == name }?.key?.success
                    ?: "No context with name $name exists.".failure
            }
            .flatMap { id -> localQueries.setCurrentContext(id).map { id } }
            .onSuccess { id -> terminal.success("Context $name ($id) is now active.") }
            .onFailure { terminal.warning(it) }
    }
}
