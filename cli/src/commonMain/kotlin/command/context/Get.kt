package command.context

import com.github.ajalt.clikt.core.CliktCommand
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking

class Get(
    private val localQueries: ILocalQueries,
    private val cacheableQueries: ICacheableQueries,
) : CliktCommand(help = "Get the current context") {

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .flatMap { id -> cacheableQueries.getContextName(id).map { id to it } }
            .onSuccess { (id, name) -> terminal.println("$name ($id)") }
            .onFailure(terminal::warning)
    }
}
