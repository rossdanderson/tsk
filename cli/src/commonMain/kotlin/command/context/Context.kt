package command.context

import com.github.ajalt.clikt.core.NoOpCliktCommand
import com.github.ajalt.clikt.core.subcommands
import db.ICacheableQueries
import db.ILocalQueries
import db.IRemoteQueries

class Context(
    localQueries: ILocalQueries,
    cacheableQueries: ICacheableQueries,
    remoteQueries: IRemoteQueries,
) : NoOpCliktCommand(help = "Manage contexts", printHelpOnEmptyArgs = true) {
    init {
        subcommands(
            listOf(
                Add(remoteQueries),
                Switch(localQueries, cacheableQueries),
                Get(localQueries, cacheableQueries),
                List(localQueries, cacheableQueries),
                Rename(localQueries, remoteQueries),
            )
        )
    }

    override fun aliases(): Map<String, kotlin.collections.List<String>> = mapOf(
        "mk" to listOf("add"),
        "ls" to listOf("list"),
    )
}
