package command.context

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.table.Borders.NONE
import com.github.ajalt.mordant.table.table
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking

class List(
    private val localQueries: ILocalQueries,
    private val cacheableQueries: ICacheableQueries,
) : CliktCommand(help = "List available contexts") {

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .flatMap { currentContext -> cacheableQueries.getContextNames().map { contexts -> currentContext to contexts } }
            .onSuccess {
                terminal.println(
                    table {
                        tableBorders = NONE
                        cellBorders = NONE
                        header {
                            style(bold = true)
                            row {
                                cell("Current")
                                cell("ID")
                                cell("Name")
                            }
                        }
                        body {
                            it.second.entries.sortedBy { it.value.value }.forEach { context ->
                                row {
                                    cell(if (context.key == it.first) "*" else "")
                                    cell(context.key)
                                    cell(context.value)
                                }
                            }
                        }
                    }
                )
            }
            .onFailure { terminal.warning(it) }
    }
}
