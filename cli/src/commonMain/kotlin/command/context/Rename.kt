package command.context

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.check
import command.terminal
import db.*
import kotlinx.coroutines.runBlocking

class Rename(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Rename the active context", printHelpOnEmptyArgs = true) {

    private val name by argument(help = "A name for the context")
        .check("value can only contain letters, numbers and -") { Regex("^[a-zA-Z0-9\\-]+$").matches(it) }

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .onSuccess { id ->
                remoteQueries.renameContext(id, ContextName(name))
                    .onSuccess { fromName -> terminal.success("Context $fromName ($id) has been renamed to $name.") }
                    .onFailure { terminal.warning(it) }
            }
            .onFailure { terminal.warning(it) }
    }
}
