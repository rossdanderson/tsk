package command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.mordant.terminal.YesNoPrompt
import db.ILocalQueries
import db.IRemoteQueries
import db.onFailure
import db.onSuccess
import kotlinx.coroutines.runBlocking

class Delete(
    private val localQueries: ILocalQueries,
    private val remoteQueries: IRemoteQueries,
) : CliktCommand(help = "Delete one or more tasks", printHelpOnEmptyArgs = true) {

    private val ids by argument(help = "The IDs of the tasks to delete").id().multiple(required = true)

    override fun run(): Unit = runBlocking {
        localQueries.getCurrentContext()
            .onSuccess { contextName ->
                ids.forEach { id ->
                    remoteQueries.getTask(contextName, id)
                        .onSuccess { task ->
                            if (YesNoPrompt(
                                    "Are you sure you wish to delete task $id '${task.description}'?",
                                    terminal
                                ).ask() == true) {
                                remoteQueries.deleteTask(contextName, id)
                                    .onSuccess { terminal.success("Task $id deleted.") }
                                    .onFailure { terminal.warning(it) }
                            }
                        }
                        .onFailure { terminal.warning(it) }
                }
            }
            .onFailure { terminal.warning(it) }
    }
}
