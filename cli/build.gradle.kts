plugins {
    kotlin("multiplatform") version "1.9.20"
    kotlin("plugin.serialization") version "1.9.20"
}

val cliktVersion: String by project

kotlin {
    linuxX64()
    mingwX64()
    macosArm64()
    macosX64()

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":core"))
                api("com.github.ajalt.clikt:clikt:$cliktVersion")
                implementation("com.github.ajalt.mordant:mordant:2.0.0-beta13")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
    }
}
