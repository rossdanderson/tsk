package parse

import common.Due.Date
import common.Due.DateTime
import common.systemTimeZone
import kotlinx.datetime.*
import kotlinx.datetime.DateTimeUnit.Companion.DAY
import kotlinx.datetime.DateTimeUnit.Companion.MONTH
import kotlinx.datetime.DateTimeUnit.Companion.WEEK
import kotlinx.datetime.DateTimeUnit.Companion.YEAR
import kotlinx.datetime.DayOfWeek.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class DueParserTest {

    private val clock = object : Clock {
        override fun now(): Instant = Instant.parse("2023-05-30T12:11:00Z")
    }

    @Test
    fun today() {
        assertEquals(clock.todayIn(systemTimeZone), (DueParser().parse("today", clock) as? Date)?.value)
    }

    @Test
    fun tomorrow() {
        assertEquals(
            clock.todayIn(systemTimeZone).plus(1, DAY),
            (DueParser().parse("tomorrow", clock) as? Date)?.value
        )
    }

    @Test
    fun days() {
        sequenceOf("d", "day", "days", "Days").forEach { suffix ->
            sequenceOf("", " ", "  ").forEach { spacing ->
                val invalid = "-1$spacing$suffix"
                println(invalid)
                assertEquals(
                    null,
                    (DueParser().parse(invalid, clock) as? Date)?.value
                )

                val empty = "$spacing$suffix"
                println(empty)
                assertEquals(
                    null,
                    (DueParser().parse(empty, clock) as? Date)?.value
                )

                sequenceOf(0, 1, 10).forEach { amount ->
                    val valid = "$amount$spacing$suffix"
                    println(valid)
                    assertEquals(
                        clock.todayIn(systemTimeZone).plus(amount, DAY),
                        (DueParser().parse(valid, clock) as? Date)?.value
                    )
                }
            }
        }
    }

    @Test
    fun weeks() {
        sequenceOf("w", "week", "weeks", "Weeks").forEach { suffix ->
            sequenceOf("", " ", "  ").forEach { spacing ->
                val negative = "-1$spacing$suffix"
                println(negative)
                assertEquals(
                    null,
                    (DueParser().parse(negative, clock) as? Date)?.value
                )

                val empty = "$spacing$suffix"
                println(empty)
                assertEquals(
                    null,
                    (DueParser().parse(empty, clock) as? Date)?.value
                )

                sequenceOf(0, 1, 10).forEach { amount ->
                    val valid = "$amount$spacing$suffix"
                    println(valid)
                    assertEquals(
                        clock.todayIn(systemTimeZone).plus(amount, WEEK),
                        (DueParser().parse(valid, clock) as? Date)?.value
                    )
                }
            }
        }
    }

    @Test
    fun months() {
        sequenceOf("mo", "month", "months", "Months").forEach { suffix ->
            sequenceOf("", " ", "  ").forEach { spacing ->
                val negative = "-1$spacing$suffix"
                println(negative)
                assertEquals(
                    null,
                    (DueParser().parse(negative, clock) as? Date)?.value
                )

                val empty = "$spacing$suffix"
                println(empty)
                assertEquals(
                    null,
                    (DueParser().parse(empty, clock) as? Date)?.value
                )

                sequenceOf(0, 1, 10).forEach { amount ->
                    val valid = "$amount$spacing$suffix"
                    println(valid)
                    assertEquals(
                        clock.todayIn(systemTimeZone).plus(amount, MONTH),
                        (DueParser().parse(valid, clock) as? Date)?.value
                    )
                }
            }
        }
    }

    @Test
    fun years() {
        sequenceOf("y", "year", "years", "Years").forEach { suffix ->
            sequenceOf("", " ", "  ").forEach { spacing ->
                val negative = "-1$spacing$suffix"
                println(negative)
                assertEquals(
                    null,
                    (DueParser().parse(negative, clock) as? Date)?.value
                )

                val empty = "$spacing$suffix"
                println(empty)
                assertEquals(
                    null,
                    (DueParser().parse(empty, clock) as? Date)?.value
                )

                sequenceOf(0, 1, 10).forEach { amount ->
                    val valid = "$amount$spacing$suffix"
                    println(valid)
                    assertEquals(
                        clock.todayIn(systemTimeZone).plus(amount, YEAR),
                        (DueParser().parse(valid, clock) as? Date)?.value
                    )
                }
            }
        }
    }

    @Test
    fun `day of week`() {
        sequenceOf(
            MONDAY to sequenceOf("mon", "monday"),
            TUESDAY to sequenceOf("tue", "tuesday"),
            WEDNESDAY to sequenceOf("wed", "weds", "wednesday"),
            THURSDAY to sequenceOf("thu", "thurs", "thursday"),
            FRIDAY to sequenceOf("fri", "friday"),
            SATURDAY to sequenceOf("sat", "saturday"),
            SUNDAY to sequenceOf("sun", "sunday"),
        )
            .forEach { (dayOfWeek, textValues) ->
                textValues.forEach { value ->
                    println(value)

                    val result = (DueParser().parse(value, clock) as? Date)?.value
                    assertEquals(dayOfWeek, result?.dayOfWeek)
                    val daysUntil = clock.todayIn(systemTimeZone).daysUntil(result!!)
                    assertTrue("$daysUntil was not within expected range") { daysUntil in 1..7 }
                }
            }
    }

    @Test
    fun date() {
        val valid = "2022-04-01"
        println(valid)
        assertEquals(
            LocalDate(2022, 4, 1),
            (DueParser().parse(valid, clock) as? Date)?.value
        )

        val invalid1 = "202-04-01"
        println(invalid1)
        assertEquals(
            null,
            (DueParser().parse(invalid1, clock) as? Date)?.value
        )
    }

    @Test
    fun `date time`() {
        val valid = "2022-04-01 13:56"
        println(valid)
        assertEquals(
            LocalDateTime(
                year = 2022,
                monthNumber = 4,
                dayOfMonth = 1,
                hour = 13,
                minute = 56,
                second = 0,
                nanosecond = 0
            ),
            (DueParser().parse(valid, clock) as? DateTime)?.value
        )

        val invalid1 = "2023-04-01 25:01"
        println(invalid1)
        assertEquals(
            null,
            (DueParser().parse(invalid1, clock) as? DateTime)?.value
        )
    }

    @Test
    fun time() {
        val valid = "13:56"
        println(valid)
        assertEquals(
            clock.todayIn(systemTimeZone).atTime(13, 56),
            (DueParser().parse(valid, clock) as? DateTime)?.value
        )

        // Next day
        val valid2 = "11:56"
        println(valid2)
        assertEquals(
            clock.todayIn(systemTimeZone).plus(1, DAY).atTime(11, 56),
            (DueParser().parse(valid2, clock) as? DateTime)?.value
        )

        val invalid1 = "25:01"
        println(invalid1)
        assertEquals(
            null,
            (DueParser().parse(invalid1, clock) as? DateTime)?.value
        )
    }
}
