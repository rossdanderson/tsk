package db

import common.Due
import db.Maybe.None
import kotlinx.datetime.DateTimeUnit.DateBased
import kotlinx.datetime.Instant

interface IRemoteQueries: ICacheableQueries {

    suspend fun createContext(
        contextName: ContextName,
    ): Result<ID>

    suspend fun renameContext(
        contextId: ID,
        toContextName: ContextName,
    ): Result<ContextName>

    suspend fun createTask(
        contextId: ID,
        description: String,
        dueBy: Pair<Due, DateBased?>?,
    ): Result<Pair<ID, Task>>

    suspend fun editTask(
        contextId: ID,
        taskId: ID,
        description: Maybe<String> = None,
        dueBy: Maybe<Pair<Due, DateBased?>?> = None,
    ): Result<Task>

    suspend fun deleteTask(contextId: ID, taskId: ID): Result<Task>

    suspend fun completeTask(contextId: ID, taskId: ID, at: Instant): Result<Pair<Task, Pair<ID, Task>?>>

    suspend fun createTaskNote(contextId: ID, taskId: ID, at: Instant, note: String): Result<Task>

    suspend fun deleteTaskNote(contextId: ID, taskId: ID, noteId: ID): Result<Task>
}
