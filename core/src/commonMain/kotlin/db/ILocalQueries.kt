package db

interface ILocalQueries {
    suspend fun setCurrentContext(id: ID): Result<Unit>

    suspend fun getCurrentContext(): Result<ID>

    suspend fun getServerLocation(): Result<IServerLocation>
}
