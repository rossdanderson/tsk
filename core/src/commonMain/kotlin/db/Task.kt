package db

import common.Due
import db.Result.Failure
import db.Result.Success
import kotlinx.datetime.DateTimeUnit.DateBased
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

sealed interface Result<out T> {

    value class Failure(val reason: String) : Result<Nothing>

    value class Success<T>(val value: T) : Result<T>
}

inline fun <T, R> Result<T>.flatMap(block: (T) -> Result<R>): Result<R> = when (this) {
    is Failure -> this
    is Success -> block(value)
}

inline fun <T, R> Result<T>.map(block: (T) -> R): Result<R> = when (this) {
    is Failure -> this
    is Success -> block(value).success
}

inline fun <T> Result<T>.onFailure(block: (String) -> Unit): Result<T> {
    when (this) {
        is Failure -> block(reason)
        is Success -> {}
    }
    return this
}

inline fun <T> Result<T>.onSuccess(block: (T) -> Unit): Result<T> {
    when (this) {
        is Failure -> {}
        is Success -> block(value)
    }
    return this
}

inline fun <T> Result<T>.orNull(): T? = when (this) {
    is Failure -> null
    is Success -> value
}

inline fun <T> Result<T>.orThrow(): T = when (this) {
    is Failure -> throw IllegalStateException("Failure: $this")
    is Success -> value
}

val <T> T.success
    get() = Success(this)

val String.failure
    get() = Failure(this)

@Serializable
value class ID(val value: UInt) {
    override fun toString(): String {
        return value.toString()
    }
}

@Serializable
value class ContextName(val value: String) {
    override fun toString(): String {
        return value
    }
}

@Serializable
data class Contexts(
    val contexts: Map<ID, Context>,
) {
    val nextContextId
        get() = contexts.nextId

    fun addOrReplaceContext(id: ID, context: Context) = copy(contexts = contexts.toMutableMap().apply {
        put(id, context)
    })
}

@Serializable
data class Context(
    val name: ContextName,
    val tasks: Map<ID, Task> = emptyMap(),
) {
    val nextTaskId
        get() = tasks.nextId

    fun addOrReplaceTask(id: ID, task: Task) = copy(tasks = tasks.toMutableMap().apply { put(id, task) })

    fun removeTask(id: ID) = copy(tasks = tasks.toMutableMap().apply { remove(id) })
}

@Serializable
data class Note(
    val created: Instant,
    val note: String,
)

@Serializable
data class Task(
    val completed: Instant?,
    val description: String,
    val created: Instant,
    val due: Due?,
    val recur: DateBased?,
    val notes: Map<ID, Note> = emptyMap(),
) {
    val nextNoteId
        get() = notes.nextId

    fun addOrReplaceNote(id: ID, note: Note) = copy(notes = notes.toMutableMap().apply { put(id, note) })

    fun removeNote(id: ID) = copy(notes = notes.toMutableMap().apply { remove(id) })
}

val Map<ID, *>.nextId: ID
    get() = ID(keys.maxOfOrNull { it.value }?.let { it + 1u } ?: 0u)
