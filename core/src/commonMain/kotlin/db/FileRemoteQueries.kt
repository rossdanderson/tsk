package db

import common.Due
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.Instant
import kotlinx.io.buffered
import kotlinx.io.files.Path
import kotlinx.io.files.SystemFileSystem
import kotlinx.io.readString
import kotlinx.io.writeString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal class FileRemoteQueries(
    private val filePath: Path,
) : IRemoteQueries {
    private val mutex = Mutex()
    private val json = Json { prettyPrint = true }

    init {
        SystemFileSystem.createDirectories(filePath.parent!!, false)
    }

    private var oldContexts: Contexts? = null

    @OptIn(ExperimentalStdlibApi::class)
    private var contexts: Contexts =
        if (SystemFileSystem.exists(filePath)) SystemFileSystem.source(filePath).buffered()
            .use { source -> json.decodeFromString(source.readString()) }
        else {
            val id = ID(0u)
            Contexts(
                contexts = mapOf(id to Context(ContextName("default"), emptyMap()))
            ).also { data ->
                SystemFileSystem.sink(filePath).buffered()
                    .use { sink -> sink.writeString(json.encodeToString(data)) }
            }
        }

    @OptIn(ExperimentalStdlibApi::class)
    private suspend fun <T : Any?> transaction(block: suspend () -> T): T =
        if (mutex.isLocked) block()
        else mutex.withLock {
            oldContexts = contexts.copy()
            var success = false
            try {
                val result = block()
                SystemFileSystem.sink(filePath).buffered()
                    .use { sink -> sink.writeString(json.encodeToString(contexts)) }
                success = true
                result
            } finally {
                if (!success) contexts = oldContexts!!
                oldContexts = null
            }
        }

    override suspend fun createContext(
        contextName: ContextName,
    ): Result<ID> = transaction {
        if (contexts.contexts.any { it.value.name == contextName }) "Context $contextName already exists".failure
        else contexts.nextContextId.also {
            contexts = contexts.addOrReplaceContext(it, Context(contextName))
        }.success
    }

    override suspend fun renameContext(
        contextId: ID,
        toContextName: ContextName,
    ): Result<ContextName> = transaction {
        if (contexts.contexts.any { it.value.name == toContextName }) "Context $toContextName already exists".failure
        getContext(contextId).map { fromContext ->
            fromContext.copy(name = toContextName).also {
                contexts = contexts.addOrReplaceContext(contextId, it)
            }
            fromContext.name
        }
    }

    override suspend fun getContextName(contextId: ID): Result<ContextName> = getContext(contextId).map { it.name }

    override suspend fun getContextNames(): Result<Map<ID, ContextName>> = transaction {
        contexts.contexts.mapValues { it.value.name }.success
    }

    override suspend fun createTask(
        contextId: ID,
        description: String,
        dueBy: Pair<Due, DateTimeUnit.DateBased?>?,
    ): Result<Pair<ID, Task>> = transaction {
        val task = Task(
            description = description,
            due = dueBy?.first,
            recur = dueBy?.second,
            created = Clock.System.now(),
            completed = null,
            notes = emptyMap(),
        )

        getContext(contextId).map { context ->
            val id = context.nextTaskId
            contexts = contexts.addOrReplaceContext(contextId, context.addOrReplaceTask(id, task))

            id to task
        }
    }

    override suspend fun editTask(
        contextId: ID,
        taskId: ID,
        description: Maybe<String>,
        dueBy: Maybe<Pair<Due, DateTimeUnit.DateBased?>?>,
    ): Result<Task> = transaction {
        getContextAndTask(contextId, taskId).map { (context, task) ->
            val editedTask = task
                .maybeRun(description) { copy(description = it) }
                .maybeRun(dueBy) { copy(due = it?.first, recur = it?.second) }

            contexts = contexts.addOrReplaceContext(
                contextId,
                context.addOrReplaceTask(
                    taskId,
                    editedTask
                )
            )

            editedTask
        }
    }

    override suspend fun getTasks(
        contextId: ID,
    ): Result<Map<ID, Task>> = transaction { getContext(contextId).map { it.tasks } }

    override suspend fun getTask(
        contextId: ID,
        taskId: ID
    ): Result<Task> = transaction { getContextAndTask(contextId, taskId).map { it.second } }

    override suspend fun deleteTask(
        contextId: ID,
        taskId: ID
    ): Result<Task> = transaction {
        getContextAndTask(contextId, taskId)
            .onSuccess { (context, _) ->
                contexts = contexts.addOrReplaceContext(contextId, context.removeTask(taskId))
            }
            .map { it.second }
    }

    override suspend fun completeTask(
        contextId: ID,
        taskId: ID,
        at: Instant
    ): Result<Pair<Task, Pair<ID, Task>?>> = transaction {
        getContextAndTask(contextId, taskId).flatMap { (context, task) ->
            if (task.completed != null) "Task $taskId already completed.".failure
            else {
                val completedTask = task.copy(completed = at)
                    .also { newTask ->
                        contexts = contexts.addOrReplaceContext(
                            contextId,
                            context.addOrReplaceTask(taskId, newTask)
                        )
                    }

                completedTask.recur?.let { recur -> completedTask.due?.nextRecurrence(recur) }
                    ?.let { nextDue ->
                        createTask(
                            contextId = contextId,
                            description = completedTask.description,
                            dueBy = nextDue to completedTask.recur,
                        )
                    }
                    ?.map { recurringTask -> completedTask to recurringTask }
                    ?: (completedTask to null).success
            }
        }
    }

    override suspend fun createTaskNote(contextId: ID, taskId: ID, at: Instant, note: String): Result<Task> =
        transaction {
            getContextAndTask(contextId, taskId).flatMap { (context, task) ->
                val noteId = task.nextNoteId
                task.addOrReplaceNote(noteId, Note(at, note)).also { newTask ->
                    contexts = contexts.addOrReplaceContext(
                        contextId,
                        context.addOrReplaceTask(taskId, newTask)
                    )
                }.success
            }
        }

    override suspend fun deleteTaskNote(contextId: ID, taskId: ID, noteId: ID): Result<Task> =
        transaction {
            getContextAndTask(contextId, taskId).flatMap { (context, task) ->
                task.removeNote(noteId).also { newTask ->
                    contexts = contexts.addOrReplaceContext(
                        contextId,
                        context.addOrReplaceTask(taskId, newTask)
                    )
                }.success
            }
        }

    private fun getContext(contextId: ID): Result<Context> =
        contexts.contexts[contextId]?.success ?: "Context $contextId doesn't exist".failure

    private fun Context.getTask(id: ID): Result<Task> = tasks[id]?.success ?: "Task $id does not exist".failure

    private fun getContextAndTask(contextId: ID, taskId: ID) =
        getContext(contextId)
            .flatMap { context -> context.getTask(taskId).map { context to it } }
}
