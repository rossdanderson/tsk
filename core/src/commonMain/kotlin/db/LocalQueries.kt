package db

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.io.buffered
import kotlinx.io.files.Path
import kotlinx.io.files.SystemFileSystem
import kotlinx.io.readString
import kotlinx.io.writeString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal class LocalQueries : ILocalQueries {
    private val filePath = Path(homeDirectory, "local.json")
    private val json = Json { prettyPrint = true }
    private val mutex = Mutex()

    init {
        SystemFileSystem.createDirectories(filePath.parent!!, false)
    }

    private var oldLocal: Local? = null

    @OptIn(ExperimentalStdlibApi::class)
    private var local: Local =
        if (SystemFileSystem.exists(filePath)) SystemFileSystem.source(filePath).buffered()
            .use { source -> json.decodeFromString(source.readString()) }
        else {
            val id = ID(0u)
            Local(currentContextId = id)
                .also { data ->
                    SystemFileSystem.sink(filePath).buffered()
                        .use { sink -> sink.writeString(json.encodeToString(data)) }
                }
        }

    @OptIn(ExperimentalStdlibApi::class)
    private suspend fun <T : Any?> transaction(block: suspend () -> T): T =
        if (mutex.isLocked) block()
        else mutex.withLock {
            oldLocal = local.copy()
            var success = false
            try {
                val result = block()
                SystemFileSystem.sink(filePath).buffered()
                    .use { sink -> sink.writeString(json.encodeToString(local)) }
                success = true
                result
            } finally {
                if (!success) local = oldLocal!!
                oldLocal = null
            }
        }

    override suspend fun setCurrentContext(id: ID): Result<Unit> = transaction {
        local = local.copy(currentContextId = id)
        Unit.success
    }

    override suspend fun getCurrentContext(): Result<ID> = local.currentContextId.success

    override suspend fun getServerLocation(): Result<IServerLocation> = local.taskLocation.success
}
