package db

import db.IServerLocation.LocalFile
import kotlinx.io.files.Path
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.EncodeDefault.Mode.ALWAYS
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable

@OptIn(ExperimentalSerializationApi::class)
@Serializable
data class Local(
    val currentContextId: ID,
    @EncodeDefault(ALWAYS)
    val taskLocation: IServerLocation = LocalFile(Path(homeDirectory, "tsk.json").toString())
)
