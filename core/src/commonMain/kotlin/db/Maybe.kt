package db

import db.Maybe.Just
import db.Maybe.None

sealed interface Maybe<out A> {
    value class Just<A>(val value: A) : Maybe<A>

    data object None : Maybe<Nothing>
}

fun <A, R> R.maybeRun(maybe: Maybe<A>, block: R.(maybeValue: A) -> R) = when (maybe) {
    is Just -> run { block(maybe.value) }
    None -> this
}
