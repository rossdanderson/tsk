package db

interface ICacheableQueries {

    suspend fun getContextName(contextId: ID): Result<ContextName>

    suspend fun getContextNames(): Result<Map<ID, ContextName>>

    suspend fun getTasks(contextId: ID): Result<Map<ID, Task>>

    suspend fun getTask(contextId: ID, taskId: ID): Result<Task>
}
