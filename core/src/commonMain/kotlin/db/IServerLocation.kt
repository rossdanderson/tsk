package db

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed interface IServerLocation {
    @Serializable
    @SerialName("LocalFile")
    // Can't be value class due to @SerialName not being included
    data class LocalFile(val path: String) : IServerLocation
}