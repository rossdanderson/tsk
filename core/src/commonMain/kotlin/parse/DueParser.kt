package parse

import common.Due
import common.Due.Date
import common.Due.DateTime
import common.systemTimeZone
import kotlinx.datetime.*
import kotlinx.datetime.DateTimeUnit.Companion.DAY
import kotlinx.datetime.DateTimeUnit.Companion.MONTH
import kotlinx.datetime.DateTimeUnit.Companion.WEEK
import kotlinx.datetime.DateTimeUnit.Companion.YEAR
import kotlinx.datetime.DayOfWeek.*

class DueParser {

    private data class Format(
        val regex: Regex,
        val descriptions: List<String>,
        val parser: (MatchResult, Clock) -> Due,
    ) {
        constructor(regex: Regex, description: String, parser: (MatchResult, Clock) -> Due) : this(
            regex,
            listOf(description),
            parser,
        )
    }

    private val formats = listOf(
        Format(
            regex = "(?i)^today$".toRegex(),
            description = "today"
        ) { _, clock -> Date(clock.todayIn(systemTimeZone), systemTimeZone) },
        Format(
            regex = "(?i)^tomorrow$".toRegex(),
            description = "tomorrow"
        ) { _, clock ->
            Date(
                clock.todayIn(systemTimeZone).plus(1, DAY),
                systemTimeZone
            )
        },
        Format(
            regex = "(?i)^([0-9]+)\\s*d\$|^([0-9]+)\\s*days?$".toRegex(),
            descriptions = listOf("#d", "# days")
        ) { matchResult, clock ->
            val inc = matchResult.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            Date(clock.todayIn(systemTimeZone).plus(inc, DAY), systemTimeZone)
        },
        Format(
            regex = "(?i)^([0-9]+)\\s*w\$|^([0-9]+)\\s*weeks?$".toRegex(),
            descriptions = listOf("#w", "# weeks")
        ) { matchResult, clock ->
            val inc = matchResult.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            Date(clock.todayIn(systemTimeZone).plus(inc, WEEK), systemTimeZone)
        },
        Format(
            regex = "(?i)^([0-9]+)\\s*mo\$|^([0-9]+)\\s*months?$".toRegex(),
            descriptions = listOf("#mo", "# months")
        ) { matchResult, clock ->
            val inc = matchResult.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            Date(clock.todayIn(systemTimeZone).plus(inc, MONTH), systemTimeZone)
        },
        Format(
            regex = "(?i)^([0-9]+)\\s*y\$|^([0-9]+)\\s*years?$".toRegex(),
            descriptions = listOf("#y", "# years")
        ) { matchResult, clock ->
            val inc = matchResult.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            Date(clock.todayIn(systemTimeZone).plus(inc, YEAR), systemTimeZone)
        },
        Format(
            regex = "(?i)^mon$|^monday$".toRegex(),
            descriptions = listOf("mon", "monday"),
            parser = shiftToDay(MONDAY),
        ),
        Format(
            regex = "(?i)^tues?\$|^tuesday\$".toRegex(),
            descriptions = listOf("tue", "tuesday"),
            parser = shiftToDay(TUESDAY),
        ),
        Format(
            regex = "(?i)^weds?\$|^wednesday\$".toRegex(),
            descriptions = listOf("wed", "wednesday"),
            parser = shiftToDay(WEDNESDAY),
        ),
        Format(
            regex = "(?i)^thu(rs)?\$|^thursday\$".toRegex(),
            descriptions = listOf("thu", "thursday"),
            parser = shiftToDay(THURSDAY),
        ),
        Format(
            regex = "(?i)^fri\$|^friday\$".toRegex(),
            descriptions = listOf("fri", "friday"),
            parser = shiftToDay(FRIDAY),
        ),
        Format(
            regex = "(?i)^sat\$|^saturday\$".toRegex(),
            descriptions = listOf("sat", "saturday"),
            parser = shiftToDay(SATURDAY),
        ),
        Format(
            regex = "(?i)^sun\$|^sunday\$".toRegex(),
            descriptions = listOf("sun", "sunday"),
            parser = shiftToDay(SUNDAY),
        ),
        Format(
            regex = "(?i)^([0-1]?[0-9]|2[0-3]):([0-5][0-9])$".toRegex(),
            description = "hh:mm"
        ) { matchResult, clock ->
            val groups = matchResult.groupValues.drop(1)
            val hour = groups[0].toInt()
            val minute = groups[1].toInt()
            val today = clock.todayIn(systemTimeZone)
            val timeToday = today.atTime(hour, minute)
            val time = if (timeToday.toInstant(systemTimeZone) > clock.now()) timeToday
            else today.plus(1, DAY).atTime(hour, minute)
            DateTime(time, systemTimeZone)
        },
        Format(
            regex = "(?i)^\\d{4}-\\d{2}-\\d{2}$".toRegex(),
            description = "yyyy-MM-dd"
        ) { matchResult, _ -> Date(LocalDate.parse(matchResult.value), systemTimeZone) },
        Format(
            regex = "(?i)^(\\d{4}-\\d{2}-\\d{2}) ([0-1]?[0-9]|2[0-3]):([0-5][0-9])$".toRegex(),
            description = "yyyy-MM-dd hh:mm"
        ) { matchResult, _ ->
            val groups = matchResult.groupValues.drop(1)
            val date = groups[0]
            val hour = groups[1].toInt()
            val minute = groups[2].toInt()
            DateTime(LocalDate.parse(date).atTime(hour, minute), systemTimeZone)
        }
    )

    val descriptions = formats.flatMap(Format::descriptions)

    private fun shiftToDay(dayOfWeek: DayOfWeek): (MatchResult, Clock) -> Date = { _, clock ->
        var shiftedDate = clock.todayIn(systemTimeZone).plus(1, DAY)
        while (shiftedDate.dayOfWeek != dayOfWeek) shiftedDate = shiftedDate.plus(1, DAY)
        Date(shiftedDate, systemTimeZone)
    }

    fun parse(string: String, clock: Clock): Due? = formats.asSequence()
        .mapNotNull { format ->
            format.regex.matchEntire(string)?.let { runCatching { format.parser(it, clock) }.getOrNull() }
        }
        .firstOrNull()
}
