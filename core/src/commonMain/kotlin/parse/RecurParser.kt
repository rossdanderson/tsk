package parse

import kotlinx.datetime.DateTimeUnit.*

class RecurParser {

    private data class Format(
        val regex: Regex,
        val descriptions: List<String>,
        val parser: (MatchResult) -> DateBased,
    ) {
        constructor(regex: Regex, description: String, parser: (MatchResult) -> DateBased) : this(
            regex,
            listOf(description),
            parser
        )
    }

    private val formats = listOf(
        Format(
            regex = "^([0-9]+)d\$|^([0-9]+) days?$".toRegex(),
            descriptions = listOf("#d", "# days")
        ) {
            val inc = it.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            DayBased(inc)
        },
        Format(
            regex = "^([0-9]+)w\$|^([0-9]+) weeks?$".toRegex(),
            descriptions = listOf("#w", "# weeks")
        ) {
            val inc = it.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            DayBased(inc * 7)
        },
        Format(
            regex = "^([0-9]+)mo\$|^([0-9]+) months?$".toRegex(),
            descriptions = listOf("#mo", "# months")
        ) {
            val inc = it.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            MonthBased(inc)
        },
        Format(
            regex = "^([0-9]+)y\$|^([0-9]+) years?$".toRegex(),
            descriptions = listOf("#y", "# years")
        ) {
            val inc = it.groupValues.drop(1).first { group -> group.isNotBlank() }.toInt()
            MonthBased(inc * 12)
        },
        Format(
            regex = "^daily$".toRegex(),
            description = "daily"
        ) { DayBased(1) },
        Format(
            regex = "^weekly$".toRegex(),
            description = "weekly"
        ) { DayBased(7) },
        Format(
            regex = "^fortnightly$".toRegex(),
            description = "fortnightly"
        ) { DayBased(14) },
        Format(
            regex = "^monthly$".toRegex(),
            description = "monthly"
        ) { MonthBased(1) },
        Format(
            regex = "^yearly$".toRegex(),
            description = "yearly"
        ) { MonthBased(12) },
    )

    val descriptions = formats.flatMap(Format::descriptions)

    fun parse(string: String): DateBased? = formats.asSequence()
        .mapNotNull { format ->
            format.regex.matchEntire(string)?.let { runCatching { format.parser(it) }.getOrNull() }
        }
        .firstOrNull()
}
