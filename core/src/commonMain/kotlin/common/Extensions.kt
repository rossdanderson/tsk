package common

import kotlinx.datetime.*
import kotlinx.datetime.DateTimeUnit.DayBased
import kotlinx.datetime.DateTimeUnit.MonthBased
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.nanoseconds

val systemTimeZone = TimeZone.currentSystemDefault()

fun DateTimePeriod.toHumanString(
    maxParts: Int = -1
): String {
    check(maxParts >= -1)
    check(maxParts != 0)

    var remParts = maxParts
    fun returnEarly() = remParts != -1 && --remParts == 0

    val includeTime = this@toHumanString !is DatePeriod

    return buildList {
        if (years != 0) {
            add("${years}y")
            if (returnEarly()) return@buildList
        }

        if (months != 0) {
            add("${months}mo")
            if (returnEarly()) return@buildList
        }

        if (days != 0) {
            add("${days}d")
            if (returnEarly()) return@buildList
        }

        if (includeTime) {
            if (hours != 0) {
                add("${hours}h")
                if (returnEarly()) return@buildList
            }

            if (hours != 0 || minutes != 0) {
                add("${minutes}m")
                if (returnEarly()) return@buildList
            }

            if (minutes != 0 || seconds != 0) {
                add("${seconds}s")
            }
        }
    }.takeIf { it.isNotEmpty() }?.joinToString(" ") ?: if (includeTime) "Now" else "Today"
}

fun DateTimeUnit.toHumanString(
    maxParts: Int = -1
): String {

    check(maxParts >= -1)
    check(maxParts != 0)

    var remParts = maxParts
    fun returnEarly() = remParts != -1 && --remParts == 0

    return when (this) {
        is DateTimeUnit.TimeBased -> buildList {
            var remDuration = nanoseconds.nanoseconds

            val intHours = remDuration.inWholeHours
            if (intHours > 0) {
                add("${intHours}h")
                if (returnEarly()) return@buildList
            }
            remDuration -= intHours.hours

            val intMinutes = remDuration.inWholeMinutes
            if (intMinutes > 0 || intHours > 0) {
                add("${intMinutes}m")
                if (returnEarly()) return@buildList
            }
            remDuration -= intMinutes.minutes

            val intSeconds = remDuration.inWholeSeconds
            if (intSeconds > 0 || intMinutes > 0) add("${intSeconds}s") else add("now")
        }

        is DayBased -> listOf("${days}d")
        is MonthBased -> buildList {
            val years = months / 12
            if (years != 0) {
                add("${years}y")
                if (returnEarly()) return@buildList
            }

            val remMonths = months % 12
            if (remMonths != 0) add("${remMonths}mo")
        }
    }.joinToString(" ")
}

fun Instant.toHumanString() = Instant.fromEpochMilliseconds((toEpochMilliseconds() / 1000) * 1000)
    .toLocalDateTime(systemTimeZone)
    .toString()

fun LocalDateTime.toHumanString() = toInstant(systemTimeZone).toHumanString()
