package common

import kotlinx.datetime.*
import kotlinx.datetime.DateTimeUnit.*
import kotlinx.datetime.DateTimeUnit.Companion.DAY
import kotlinx.datetime.DateTimeUnit.Companion.MONTH
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.time.Duration.Companion.nanoseconds

@Serializable
sealed interface Due {

    val timeZone: TimeZone

    fun nextRecurrence(dateTimeUnit: DateBased): Due

    val completeBy: Instant

    @Serializable
    @SerialName("Date")
    data class Date(
        val value: LocalDate,
        override val timeZone: TimeZone,
    ) : Due {
        override val completeBy: Instant = value.plus(1, DAY).atStartOfDayIn(timeZone) - 1.nanoseconds

        override fun nextRecurrence(dateTimeUnit: DateBased): Due {

            val increment: LocalDate.() -> LocalDate = when (dateTimeUnit) {
                is DayBased -> {
                    { plus(dateTimeUnit.days, DAY) }
                }

                is MonthBased -> {
                    { plus(dateTimeUnit.months, MONTH) }
                }
            }

            val today = Clock.System.todayIn(timeZone)
            var nextDue: LocalDate = value.increment()
            while (today >= nextDue) nextDue = nextDue.increment()
            return Date(nextDue, timeZone)
        }
    }

    @Serializable
    @SerialName("DateTime")
    data class DateTime(
        val value: LocalDateTime,
        override val timeZone: TimeZone,
    ) : Due {
        override val completeBy: Instant = value.toInstant(timeZone)

        override fun nextRecurrence(dateTimeUnit: DateBased): Due {
            val increment: Instant.() -> Instant = when (dateTimeUnit) {
                is DayBased -> {
                    { plus(dateTimeUnit.days, DAY, timeZone) }
                }

                is MonthBased -> {
                    { plus(dateTimeUnit.months, MONTH, timeZone) }
                }
            }

            val now = Clock.System.now()
            var nextDue: Instant = value.toInstant(timeZone)
            while (now >= nextDue) nextDue = nextDue.increment()
            return DateTime(nextDue.toLocalDateTime(timeZone), timeZone)
        }
    }
}
