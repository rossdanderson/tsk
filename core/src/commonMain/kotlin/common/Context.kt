package common

import db.*
import kotlinx.coroutines.runBlocking
import kotlinx.io.files.Path
import parse.DueParser
import parse.RecurParser

object Context {
    val localQueries: ILocalQueries = LocalQueries()
    val remoteQueries: IRemoteQueries = runBlocking {
        when (val location = localQueries.getServerLocation().orThrow()) {
            is IServerLocation.LocalFile -> FileRemoteQueries(Path(location.path))
        }
    }
    val dueParser = DueParser()
    val recurParser = RecurParser()
}