@file:OptIn(ExperimentalForeignApi::class)

package db

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.toKString
import kotlinx.io.files.Path
import platform.posix.getenv

actual val homeDirectory: Path = Path(getenv("LOCALAPPDATA")?.toKString()!!, "tsk")